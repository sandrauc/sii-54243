// Logger.cpp: implementation of the Logger class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(){
	int fd;
	char cad_fifo[200];
	
	if(mkfifo("puntos", 0766) < 0){
		//perror("mkfifo");
		//return 1;
	}
	
	fd = open("puntos", O_RDONLY);
	if(fd < 0){
		perror("open");
		return 1;
	}
	
	while(read(fd, &cad_fifo, sizeof(cad_fifo)) > 0){
		printf("%s", cad_fifo);
	}
	
	close(fd);
	unlink("puntos");
	return 0;
}
