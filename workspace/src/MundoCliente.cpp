// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	munmap(p_datos, sizeof(DatosMemCompartida));
	close(fd_ser_cli);
	unlink("FIFO_ser_cli");
	close(fd_cli_ser);
	unlink("FIFO_cli_ser");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);

	for(int i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	for(int j=0; j<esferas.size(); j++){
		esferas[j].Dibuja();
	}
	
	for(int i=0; i<disparos.size(); i++){
		disparos[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	
	//Leemos datos tuberia servidor cliente
	char cad[200];
	read(fd_ser_cli, cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);  
	
	//Actualizamos memoria compartida
	p_datos->esfera = esfera;
	p_datos->raqueta1 = jugador1;
	
	if(p_datos->accion == 1){
		OnKeyboardDown('w', 0, 0);
	}
	else if(p_datos->accion == -1){
		OnKeyboardDown('s', 0, 0);
	}
	
	if(puntos1 >= 3){
		exit(1);
	}
	else if (puntos2 >= 3){
		exit(1);
	}
	
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'b':
		esferas.push_back(*(new Esfera));
		break;
		
	case 'd':
		disparos.push_back(*(new Disparo(0, jugador1.getPos())));
		break;
	case 'k':
		disparos.push_back(*(new Disparo(1, jugador2.getPos())));
		break;

	}
	
	//Pasamos los datos a la FIFO cliente servidor
	write(fd_cli_ser, &key, sizeof(key));
}

void CMundoCliente::Init()
{
	
	//FIFO servidor cliente
	mkfifo("FIFO_ser_cli", 0766);
	fd_ser_cli = open("FIFO_ser_cli", O_RDONLY);
	if(fd_ser_cli < 0){
		perror("open fifo servidor cliente");
		return;
	}
	
	//FIFO cliente servidor
	mkfifo("FIFO_cli_ser", 0766);
	fd_cli_ser = open("FIFO_cli_ser", O_WRONLY);
	if(fd_cli_ser < 0){
		perror("open fifo cliente servidor");
		return;
	}
	
	//Memoria compartida
	int fd_mem;
	fd_mem = open("DatosMemCompartida", O_CREAT|O_TRUNC|O_RDWR, 0766);
	if(fd_mem < 0){
		perror("open");
		return;
	}
	datos.esfera = esfera;
	datos.raqueta1 = jugador1;
	datos.accion = 0;
	ftruncate(fd_mem, sizeof(DatosMemCompartida));
	write(fd_mem, &datos, sizeof(datos));
	p_datos = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(datos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mem, 0));
	if(p_datos == MAP_FAILED){
		perror("mmap");
		return;
	}
	close(fd_mem);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
