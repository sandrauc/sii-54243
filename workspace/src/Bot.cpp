#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>


int main(){
	DatosMemCompartida* datos;
	int fd;
	float RaquetaPos;
	
	fd = open("DatosMemCompartida", O_RDWR);
	if(fd < 0){
		perror("open");
		return 1;
	}
	datos = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	close(fd);
	
	while(1){
		RaquetaPos = (datos->raqueta1.y1 + datos->raqueta1.y2) / 2;
		if((datos->esfera.centro.y) > (RaquetaPos)){
			datos->accion = 1;
		}
		else if((datos->esfera.centro.y) < (RaquetaPos)){
			datos->accion = -1;
		}
		else{
			datos->accion = 0;
		}
		usleep(25000);
	}
	munmap(datos, sizeof(DatosMemCompartida));
	return 0;
}
