// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{

}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t) {
	y1 = y1 + velocidad.y * t;
	y2 = y2 + velocidad.y * t;
}

void Raqueta::Reducir(){
	if((y2 - y1) > 1) {
		y1 += 0.1;
		y2 -= 0.1;
	}
}
