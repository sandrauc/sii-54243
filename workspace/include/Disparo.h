//Autor: Sandra Ures Ciriaco
// Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

//#include "Esfera.h"

#ifndef DISPARO_H
#define DISPARO_H

#if _MSC_VER > 10000
#pragma once
#endif //_MSC_VER > 1000

#include "Vector2D.h"

class Disparo {
private:	
	int sentido;//0: hacia derecha 1: hacia izquierda
public:
	Vector2D centro;
	Vector2D velocidad;
	float radio;

	Disparo(int s, Vector2D pos);
	virtual ~Disparo();
	void Dibuja();
	void Mueve(float t);
};


#endif
