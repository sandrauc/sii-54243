#ifndef DATOSMEMCOMPARTIDA_H
#define DATOSMEMCOMPARTIDA_H

#if _MSC_VER > 10000
#pragma once
#endif //_MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida {       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
};

#endif
