//Autor: Sandra Ures Ciriaco
// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RAQUETA_H
#define RAQUETA_H

#if _MSC_VER > 10000
#pragma once
#endif //_MSC_VER > 1000

#include "Plano.h"
//#include "Vector2D.h"
//#include "Disparo.h"

class Raqueta : public Plano {
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	void Reducir();
};

#endif
